// [SECTION] Introduction to Arrays

	// practice: create/declare multiple variables
	let student1 = "Martin";	
	let student2 = "Maria";
	let student3 = "John";
	let student4 = "Ernie";
	let student5 = "Bert";

	// store the following values inside a single container.
	// to declare an array => we simply use an 'array literal' or square bracket '[]'
	// use '=' => assignment operator to repackage the structure inside the variable
	// be careful when selecting variable names.
	let batch = ['Martin', 'Maria', 'John', 'Ernie', 'Bert'];
	console.log(batch);

	// create an array that will contain different computerBrands
	let computerBrands = ['Asus', 'Dell', 'Apple', 'Acer', 'Toshiba', 'Fujitsu'];

	console.log(computerBrands);
		// '' or "" => both are used to declare a string data type in JS.
		// [SIDE LESSON]
		/*
			'apple' === "apple"

			How to Choose if single quote or double quote?
			Use case of the data:
			If you're going to use quotations, dialog inside a string.

		*/
		let message = "Using Javascript's Array Literal";
		console.log(message);

		let lastWords = '"I Shall Return!" - MacArthur';
		console.log(lastWords);

		// NOTE: when selecting the correct quotation type, you cna use them as an [ESCAPE TOOL] to properly declare expressions within a string.

		// [ALTERNATIVE APPROACH] "\" backward slash to insert quotations.

		// example:
		let message2 = 'Using Javascript\'s Array Literal';
		//  ('\') - this can be used to escape both a single/double quotation expressions.

		// NOTE: A lot of developers prefer the use of '' (single quotations) as it is easier/simpler to use when declaring string data. (AGAIN, take note of the use cases.)



